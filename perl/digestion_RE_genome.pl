#!/usr/bin/perl
use strict;
use warnings;

require v5.10;
use feature qw(say switch);

#
#  Harmen van de Werken : harmen.vandewerken@gmail.com
#
# ./digestion_RE_genome.pl -v -i /data/genomes/mmusculus/mm10/merge/mm10_merge.fa -o /tmp/ -re1 HindIII -re2 NlaIII
## or ~/projects/general/perl/make_re_bed.pl -i /data/genomes/mmusculus/mm9/merge/mm9_merge.fa -r1 ApoI -o /data/genomes/mmusculus/mm9/enzdb/ 
## ~/projects/THiC/perl/digestion_RE_genome.pl -i /data/genomes/mmusculus/mm9/merge/mm9_merge.fa -o /data/genomes/mmusculus/mm9/enzdb/ -re1 HindIII -re2 DpnII -v 

use Getopt::Long qw(:config no_ignore_case);
use Bio::SeqIO; # Bioperl modules
use Bio::Restriction::EnzymeCollection;
use Bio::Restriction::Analysis;
use File::Basename;
use File::Copy;
use File::Path qw(make_path);
use lib "/home/harmen/perl/lib/";
#use lib "/home/harmen/perl/4C_mapping/lib/";
use HarmenGeneral;
our $VERSION = '1.00';


my $ifa; # sequenece file
my $odir; # ouput directory
my $re1;
my $re2;
my $v; #verbose
my $V; #Version
my $h; #help
my $perlpr = '/home/harmen/perl/4C_mapping/';

GetOptions(
    "i|ifa=s" => \$ifa,
    "o|odir=s" => \$odir,
    "r1|re1=s" => \$re1,
    "r2|re2:s" => \$re2,
    "v|verbose"=>\$v, # flag
    "V|version"=>\$V, # flag
    "h|help|?" => \$h);

if ($V) {
        print "\n$0 v$VERSION\n\n";
        exit 0;
        }
usage() if $h;

my $time;
if ($v) { use Time::Elapsed qw(elapsed);
        $time =time()};

usage() unless defined($ifa);
usage() unless defined($odir);
usage() unless defined($re1);

say "### $0 started: ". localtime() . " ###" if $v;
my $build = buildf(\$ifa);
$odir = dir_one_fw_slash $odir;

say "build: ",buildf(\$ifa) if $v;

make_path($odir, {
    verbose => 1,
    mode => 0711,
});

my $enz_col = Bio::Restriction::EnzymeCollection->new;
my $enz_re1 = $enz_col->get_enzyme($re1);
die "Unknown restriction enzyme $re1\n" unless defined $enz_re1;
my $enz_re1_site = $enz_re1->seq->seq();
my $enz_re1_reexp = $enz_re1->recog;
my $enz_re1_length = $enz_re1->recognition_length;
my $red1 = $enz_re1->cut;
$red1 += length($enz_re1->overhang_seq) if $enz_re1->overhang eq "5'";
say "First Enzyme $re1: $enz_re1_site Regular expression $enz_re1_reexp" if $v;


my $fdg =  $odir."$build"."_".$enz_re1->recog.".gz";

my $seqobj = Bio::SeqIO->new(-file   => "<$ifa",
			     -format => 'fasta' );
my $f = 1;
my $fe = 1;

my $prog = $perlpr. 'first_cutter2.pl';
my $gz = $fdg;
if  (-e $gz) {
    say "$gz already exists no new file";
} else {
    say $fdg if $v;
    system_call("perl $prog -i $ifa -r $enz_re1_reexp -o $fdg\~ ");
    move($gz.'~', $gz) or die "cannot move $gz\~: $!\n";
}

unless ($re2)  {
    printf "### $0 ended after: %s on: %s ###\n", elapsed(time()-$time), scalar(localtime()),"\n" if $v;
    exit 0;
}


my $enz_re2 = $enz_col->get_enzyme($re2);
die "Unknown restriction enzyme $re2\n" unless defined $enz_re2;
my $enz_re2_site = $enz_re2->seq->seq();
my $enz_re2_reexp = $enz_re2->recog;
my $enz_re2_length = $enz_re2->recognition_length;
my $red2 = $enz_re2->cut;
$red2 += length($enz_re2->overhang_seq) if $enz_re2->overhang eq "5'";

say "Second Enzyme $re2: $enz_re2_site Regular expression $enz_re2_reexp" if $v;

my $sdg =  $odir."$build"."_".$enz_re1->recog."_".$enz_re2->recog.".gz";
(my $bed = $sdg) =~ s/\.gz/\.bed\.gz/;

$prog = $perlpr. 'second_cutter2.pl';
$gz = $sdg;
if  (-e $gz) {
    say "$gz already exists no new file";
} else {
    say $sdg if $v;
    system_call("perl $prog -i $fdg -r1 $enz_re1_reexp -re2 $enz_re2_reexp -o $sdg\~ ");
    move($gz.'~', $gz) or die "cannot move $gz\~: $!\n";
}

if  (-e $bed) {
    say "$bed already exists no new file";
} else {
    open(IN, "gunzip -c $sdg |") or die "can't open pipe to $sdg";
    open OUT, "| gzip > $bed\~"  or die "Cannot create file $bed\~: $!";
    while (<IN>) {
	if ($. == 1) {
	    say OUT "track name=\"$build","_",$re1,"_",$re2,"\"  description=\"$build","_",$re1,"_",$re2,"\"";
	} else { 
	    my @line = split("\t");
	    say OUT join("\t", $line[2],$line[7] -1 ,$line[8]) unless ($line[11] == 1 and $line[9] eq "-");}
#	    say OUT join("\t", $line[2],@line[7..8],$line[0],$line[11],$line[9]) unless ($line[11] == 1 and $line[9] eq "-");}
    }
    move($bed.'~', $bed) or die "cannot move $bed\~: $!\n";
}


printf "### $0 ended after: %s on: %s ###\n", elapsed(time()-$time), scalar(localtime()),"\n" if $v;
exit 0;

sub buildf {
    ## return dir/mm10_..... return mm10
    my $dir  = shift;
    my @dir;
    if (ref($dir)) {
	@dir = split("/", $$dir);
    }
    else {
	@dir =  split("/", $dir);
    }
    my @file = split("_", $dir[$#dir]);
    return ($file[0]);
}

sub usage {
print STDERR <<USAGE;
usage:  $0 v$VERSION -i fastfile -o output directory [-v|--verbose] [-V|--version] [-h|--help|?] 
        -i : data_index file
        -h or ? : help
        -v : verbose output
        -V : version
USAGE
exit(0);
}


#  LocalWords:  cpu's
