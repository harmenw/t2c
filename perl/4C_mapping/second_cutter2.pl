#!/usr/bin/perl
use strict;
use warnings;

#
#  Harmen van de werken : harmen.vandewerken@gmail.com
#
#

use Getopt::Long qw(:config no_ignore_case);
use Bio::SeqIO; # Bioperl modules
use Bio::Perl;

# mapper library
use Cwd 'abs_path';
use File::Basename;
use lib dirname(abs_path($0)) .'/lib';
use Mapper;

our $VERSION = '1.00';

my $f_sites;# output file
my $fe_sites;# output file
my $re1;
my $re2;
my $v; #verbose
my $V; #Version
my $h; #help

GetOptions(
        "i|f_sites=s" =>\$f_sites,
        "o|fe_sites=s" =>\$fe_sites,
	"r1|re1=s" => \$re1,
	"r2|re2=s" => \$re2,
	"v|verbose"=>\$v, # flag
        "V|version"=>\$V, # flag
        "h|help|?" => \$h);

if ($V) {
        print "\n$0 v$VERSION\n\n";
        exit 0;
        }
usage() if $h;
usage() unless defined($fe_sites);

my $time;
if ($v) { use Time::Elapsed qw(elapsed);
        $time =time()};


my @fragend = qw(fragend_id fragment_id chr f_start f_end end_chr f_ambig fe_start fe_end fe_strand fe_sequence fe_blind fe_ambig fe_end_chr);
my @fragment = @fragend[0..6];
#fragend_id	fragment_id	chr	start	end	fragment_length	sequence	end_chr	NumberN	 fe_start	fe_strand fe_end	fe_sequence	fe_blind fe_NumberN


print "## $0 $fe_sites started: ". localtime() ."\n" if $v;

open INPUT, "gunzip -c $f_sites |"  or die "Cannot open file $f_sites: $!";
open OUTPUT, "| gzip >".$fe_sites  or die "Cannot create file $fe_sites: $!";

print OUTPUT join("\t",@fragend),"\n";
my $fragend_id = 0;
my $chr_old = -1;
#my $count_end_chr = 0;
my $count_fe_end;
my $fe_end;
while(<INPUT>) {
    chomp;
    next if /^frag/; #comment
    my @line = split(); #5 sequence
    $count_fe_end = 0 unless $chr_old eq $line[1];  # first chromosome
    ++$fragend_id;
    ## NA
    my $pos = 0;
    my $hitl;
    if ($re2 ne "NA") { # no second cutter therefore all sequences are blind fragments
	#
	while ( $line[4] =~ /$re2/g) {
	    $pos = pos($line[4]);
	    last;
	}
    }
    if ($pos == 0) { # blind fragment 
	++$count_fe_end if $line[5] == 1;
	if ($count_fe_end == 1 or $count_fe_end == 4) { $fe_end = 1 }else{ $fe_end = 0}
	print OUTPUT join("\t",$fragend_id,$line[0],$line[1],$line[2],$line[3],$line[5],$line[6],$line[2],$line[3],'+',$line[4],'1',$line[6],$fe_end),"\n";
	++$fragend_id;
	++$count_fe_end if $line[5] == 1; 
	# end of chromosome when also if chromosome was not cut by first cutter
	if ($count_fe_end == 1 or $count_fe_end == 4 or ($count_fe_end == 2 and $line[4] !~ /$re1/)) { $fe_end = 1 }else{ $fe_end = 0}
	print OUTPUT join("\t",$fragend_id,$line[0],$line[1],$line[2],$line[3],$line[5],$line[6],$line[2],$line[3],'-',revcom_as_string($line[4]),'1',$line[6],$fe_end),"\n";
    } else {
	my $fragend_seq = substr($line[4],0,$pos);
	my $fe_ambig = 0;
	if ($line[6] > 0){ # fragment ambiguous
	    $fe_ambig = ambig(\$fragend_seq)}
	++$count_fe_end if $line[5] == 1; 
	if ($count_fe_end == 1 or $count_fe_end == 4) { $fe_end = 1 }else{ $fe_end = 0}
	print OUTPUT join("\t",$fragend_id,$line[0],$line[1],$line[2],$line[3],$line[5],$line[6],$line[2],$line[2]+$pos-1,'+',$fragend_seq,'0',$fe_ambig,$fe_end),"\n";
	++$fragend_id;
	my $revseq = revcom_as_string($line[4]);
	$revseq =~ /$re2/g;
	$pos = pos($revseq);
	$fragend_seq = substr($revseq,0,$pos);
	$fe_ambig = 0;
	if ($line[6] > 0){ # fragment ambiguous
	    $fe_ambig = ambig(\$fragend_seq)}
	++$count_fe_end if $line[5] == 1; 
	# end of chromosome when also if chromosome was not cut by first cutter
	if ($count_fe_end == 1 or $count_fe_end == 4 or ($count_fe_end == 2 and $line[4] !~ /$re1/)) { $fe_end = 1 }else{ $fe_end = 0}
	print OUTPUT join("\t",$fragend_id,$line[0],$line[1],$line[2],$line[3],$line[5],$line[6],$line[3]-$pos+1,$line[3],'-',$fragend_seq,'0',$fe_ambig,$fe_end),"\n";
    }
    $chr_old=$line[1];
}
close INPUT  or die "Cannot close file $f_sites: $!";
close OUTPUT  or die "Cannot close file $fe_sites: $!";

printf "## $0 $fe_sites ended after: %s \n", elapsed(time()-$time)," on :", scalar(localtime()),"\n" if $v;
exit(0);


sub usage {
print STDERR <<USAGE;
usage:  $0 v$VERSION -i file [-v|--verbose] [-V|--version] [-h|--help|?]
        -i : file
        -h or ? : help
        -v : verbose output
        -V : version
USAGE
exit(0);
}
