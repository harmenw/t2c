#!/usr/bin/perl
use strict;
use warnings;

require v5.10;
use feature qw(say);

#
#  Harmen van de werken : harmen.vandewerken@gmail.com
#
#

use Getopt::Long qw(:config no_ignore_case);
use Bio::DB::Sam;

our $VERSION = '1.00';

my $ibam;
my $nbedpos;
my $v; #verbose
my $V; #Version
my $h; #help

GetOptions(
    "i|ibam=s"=>\$ibam,
    "b|nbedpos"=>\$nbedpos, ## zero like bedtools  or 1 not like bedtools add 1 to start
    "v|verbose"=>\$v, # flag
    "V|version"=>\$V, # flag
    "h|help|?" => \$h);

if ($V) {
        print "\n$0 v$VERSION\n\n";
        exit 0;
        }
usage() if $h;
usage() unless defined($ibam);
my $time;
if ($v) { use Time::Elapsed qw(elapsed);
        $time =time()};

print "## $0 started: ". localtime() ."\n" if $v;
###
## check position
## 
my $addposstart = 0; ## how to start bed
$addposstart = 1  if ($nbedpos);
my $bam          = Bio::DB::Bam->open($ibam);

my $header       = $bam->header;
my $target_count = $header->n_targets;
my $target_names = $header->target_name;
my $i =0;
my @pealign;
while ( $pealign[0] = $bam->read1) {
    $pealign[1] = $bam->read1; ## second reverse alignment
    ++$i;
#    $tid = $align->tid( [$new_tid] )
    (my $qnamef = $pealign[0]->qname)  =~ s/#.*//;
    (my $qnamer = $pealign[1]->qname)  =~ s/#.*//;
    die " qnames not identical in bam file not sorted ",  $pealign[0]->qname, " ",  $pealign[1]->qname unless $qnamef eq $qnamer;   
    my $pe = 1;
    ### better with true sort and not alphanumeric
    if (rmchr($target_names->[$pealign[0]->tid]) gt  rmchr($target_names->[$pealign[1]->tid])  or 
	    ($target_names->[$pealign[0]->tid] eq $target_names->[$pealign[1]->tid]
		and $pealign[0]->pos  > $pealign[1]->pos)) {
	$pe = 2;
	@pealign = reverse @pealign;
    }
    
    say join("\t",
	     $target_names->[$pealign[0]->tid],
	     $pealign[0]->pos+$addposstart,
	     $pealign[0]->calend,

	     $target_names->[$pealign[1]->tid],
	     $pealign[1]->pos+$addposstart,
	     $pealign[1]->calend,

	     $qnamef,
	     $pealign[0]->qual, ## score
	     strandc($pealign[0]->strand),
	     strandc($pealign[1]->strand),
	     $pe);

}
#     my @line1 = split("\t",$line1);
#     my $line2 = <IN>;
#     my $hline1 
#     chomp($line2);   
#     my @line2 = split("\t",$line2);
#     say $line1[0];
# # perl -F"\t" -lane 'next if $F[0] =~ /#0\/3:[01]; 
# #                          my $bin=substr(unpack("B32",pack('$F[1]',$F[1])), -8);
# #                          my $st = "+";
# #                          $st = "-" if substr($bin, 3,1);
# #                          my $srev = "+";
# #                          $srev = "-" if substr($bin, 2,1);
# #                          ## move order
# #                          (my $f = $F[2]) =~ s/^chr//;
# #                          (my $s = $F[6]) =~ s/^chr//; 
# #                          my @pos = (0) x 6; 
# #                          if ( $F[6]  eq "=" )  { ## cis
# #                             if ($F[3] < $F[7] )
# #                                 @pos = ($F[3],$F[3] + 
# #                                 else {
# #                                 }
# #                             } else {                           
# #                             }                           
# #  and $F[7] < 
# }
#close IN or die "Cannot close file $ibam: $!";
#$bam->close();
printf "## $0 ended after: %s \n", elapsed(time()-$time)," on :", scalar(localtime()),"\n" if $v;
exit 0;

sub rmchr {
    my $chr = shift;
    $chr =~ s/^chr//;
    return($chr);
}

sub strandc{
    my $strand = shift;
    ($strand == 1) ? ($strand = "+") : ($strand = "-");
    return($strand);
}

sub usage {
print STDERR <<USAGE;
usage:  $0 v$VERSION -i file [-v|--verbose] [-V|--version] [-h|--help|?] [-b]
        -i : file
        -b : subtract from each position 1 
        -h or ? : help
        -v : verbose output
        -V : version
USAGE
exit(0);
}

