use Math::Round qw/nearest/;

sub ambig {
    my $seq = shift;
    return (length($$seq)- ($$seq =~ tr/ACTGactg//));
}

sub repback { #replace regex
    my $regex  = shift;
    unless ($regex eq "NA") {
	$regex =~ s/l/\(/g; # right parenthesis
	$regex =~ s/L/\[/g; # right parenthesis
	$regex =~ s/r/\)/g; # right parenthesis
	$regex =~ s/R/\]/g; # right parenthesis
	$regex =~ s/p/\|/g; # right parenthesis
	my $revcompl = _revcompl($regex);
	$regex = $regex . '|' . $revcompl unless $revcompl eq $regex;
	$regex = '(' . $regex .')';
    }
    return($regex);
}

sub _revcompl {
  my $regex = shift;
  $regex = reverse($regex);
  # translate and swap ()
  $regex  =~ tr/()[]ACGTacgt/)(][TGCAtgca/;
  return($regex);
}

sub gc_content {
    my $seq = shift;
    my $gc=($$seq=~ tr/GCgc//);
    my $tot=($$seq=~ tr/ATGCatgc//);
    my $gc_cont;
    if ($tot == 0) {
	$gc_cont = "";
	my $n=($$seq=~ tr/Nn//);
	warn("No ATGCNnatgc nucleotides found\n") if $n == 0;
    } else {
	$gc_cont = nearest(.1, 100* $gc/$tot);
    }
    return($gc_cont);
}

1;
