#!/bin/bash 
## qc
##  ~/cormorant/home/harmen/perl/Davis_test/run_davis_test.pl -d . -c -b ../../bwa/hg19_bwtsw
## goto lala for space
idir=/media/lala/harmen/data/
pushd=/media/lala/harmen/
prog=~/cormorant/home/harmen/perl/THiC/THiC_mapping.pl
dmg=${idir}T2C/run_2014_02/mm9/Beta_globin/
dmng=${idir}T2C/run_2014_02/mm9/Non_globin/
dh=${idir}T2C/run_2014_02/hg18

bm=/media/lala/harmen/data/bwa/mm9_bwtsw
bh=/media/lala/harmen/data/bwa/hg18_bwtsw

bemg=/media/lala/harmen/data/THiC/enzdb/bwa/mm9_HindIII_NlaIII_bwtsw
bemng=/media/lala/harmen/data/THiC/enzdb/bwa/mm9_HindIII_DpnII_bwtsw
beh=/media/lala/harmen/data/THiC/enzdb/bwa/hg18_BglII_NlaIII_bwtsw

## bedfiles
br1m=~/cormorant/data/genomes/mmusculus/mm9/enzdb/mm9_HindIII.bed.gz
r2rmg=~/cormorant/data/genomes/mmusculus/mm9/enzdb/mm9_NlaIII2NlaIII.bed.gz
r2rmng=~/cormorant/data/genomes/mmusculus/mm9/enzdb/mm9_DpnII2DpnII.bed.gz
br1h=~/cormorant/data/genomes/hsapiens/hg18/enzdb/hg18_BglII.bed.gz 
r2rh=~/cormorant/data/genomes/hsapiens/hg18/enzdb/hg18_NlaIII2NlaIII.bed.gz 

#quality control fastqc
# perl ${prog} -f 4 -v -t 30 -d ${dh} -b ${bh} -r BglII  -e ${beh} -v -c;#
# perl ${prog} -f 4 -v -t 30 -d ${dm} -b ${bm} -r HindIII -e ${bem} -v -c;
#perl ${prog} -f 4 -v -t 30 -d /media/lala/harmen/data/THiC/ApoI/ -b ${bm} -r ApoI -e ${bem} -v -c;
memory=$(perl -e 'print 28*1e9')
### note that -f waht happens if multiple sort !!! in PE
## human 
echo memory: ${memory}


perl ${prog} -f 4 -v -t 8 -m ${memory} -d ${dh} -b ${bh} -r BglII  -e ${beh} -br1 ${br1h} -r2r ${r2rh} -v;
## globin 
perl ${prog} -f 4 -v -t 8 -m ${memory} -d ${dmg} -b ${bm} -r HindIII -e ${bemg} -br1 ${br1m} -r2r ${r2rmg}  -v;
## non globin
perl ${prog} -f 4 -v -t 8 -m ${memory} -d ${dmng} -b ${bm} -r HindIII -e ${bemng} -br1 ${br1m} -r2r ${r2rmng} -v;

##popd
#perl ${prog} -f 8 -v -t 1 -d ${dh} -b ${bh} -r BglII  -e ${beh} -v;
#perl ${prog} -f 8 -v -t 1 -d ${dm} -b ${bm} -r HindIII -e ${bem} -v;
