#!/usr/bin/perl
use strict;
use warnings;
#
#  Harmen van de werken : harmen.vandewerken@gmail.com
#
#

use Getopt::Long qw(:config no_ignore_case);
use Bio::SeqIO; # Bioperl modules

# mapper library
use Cwd 'abs_path';
use File::Basename;
use lib dirname(abs_path($0)) .'/lib';
use Mapper;

our $VERSION = '1.00';


my $f_sites;# output file
my $ifa; # sequenece
my $regex;
my $v; #verbose
my $V; #Version
my $h; #help

GetOptions(
    "i|ifa=s" => \$ifa,
    "r|regex=s" => \$regex,
    "o|f_sites=s" =>\$f_sites,
    "v|verbose"=>\$v, # flag
    "V|version"=>\$V, # flag
    "h|help|?" => \$h);

if ($V) {
        print "\n$0 v$VERSION\n\n";
        exit 0;
        }
usage() if $h;
usage() unless defined($f_sites);

$regex = "($regex)";
$regex = qr/$regex/i;

my $time;
if ($v) { use Time::Elapsed qw(elapsed);
        $time =time()};


my @fragment = qw(fragment_id chr f_start f_end f_sequence end_chr f_ambig);
print "## $0 $f_sites started: ". localtime() ."\n" if $v;

open OUTPUT, "| gzip >".$f_sites  or die "Cannot create file $f_sites: $!";
print OUTPUT join("\t",@fragment),"\n";

my $frag_id = 0;
my $seqobj = Bio::SeqIO->new(-file   => "<$ifa",
			     -format => 'fasta' );
while (my $seq = $seqobj->next_seq) { # stream object
    my $chr = $seq->display_id();
    my $frag_seq;
    my $old_loc = 0;
    my $new_loc;
    my $chr_count =0;
    my $end_chr = 1;
    my $ref =\$seq->seq();
    while ($$ref =~ /$regex/g) {
	++$frag_id; 
	++$chr_count;
	#next if $chr_count > 10; #test
	$new_loc = pos($$ref);
	my $end_chr = 0;
	if ($chr_count==1) { # begin chromosome
	    $end_chr = 1;
	}
	$frag_seq = uc(substr($$ref,$old_loc,$new_loc-$old_loc));
	print OUTPUT join("\t",$frag_id,$chr,$old_loc+1,$new_loc,$frag_seq,$end_chr,ambig(\$frag_seq)),"\n";
	$end_chr = 0; #set to zero for all lines except the first one
	$old_loc = $new_loc-length($1);# start restriction site	    
    }
    # last fragment
    $frag_seq = uc(substr($$ref,$old_loc,length($$ref)-$old_loc));# $frag_seq =  uc($seq->subseq($old_loc,$seq->length()));
    print OUTPUT join("\t",$frag_id+1,$chr,$old_loc+1,length($$ref),$frag_seq,1,ambig(\$frag_seq)),"\n";
}
close OUTPUT  or die "Cannot close file $f_sites: $!";

printf "## $0 $f_sites ended after: %s \n", elapsed(time()-$time)," on :", scalar(localtime()),"\n" if $v;
exit(0);

sub usage {
print STDERR <<USAGE;
usage:  $0 v$VERSION -i file [-v|--verbose] [-V|--version] [-h|--help|?]
        -i : file
        -h or ? : help
        -v : verbose output
        -V : version
USAGE
exit(0);
}
