#!/bin/bash
pushd /data/THiC/Oligos_on_the_array/
fmm91=OID40169_HindIII_Runx1_mm9.probes
fmm92=OID40170_HindIII_Myb_Bcl11a.probes
for ifile in ${fmm91} ${fmm92}
do
    continue;
    echo ${ifile}
    fafile=$(echo ${ifile} | sed -e 's/\.probes$/\_seq.fa/' )
    obed=$(echo ${ifile} | sed -e 's/\.probes$/\.bed.gz/' )
    cut -f 1 ${ifile} | perl -lne 'next if $. == 1; print ">",$.-1," $_";print' > ${fafile}
    if [ ${ifile} == ${fmm91} ] ||  [ ${ifile} == ${fmm92} ]; then
	echo "mouse"
	build=mm9 
    else
	echo "human"
	build=hg19
    fi
    eval megablast -d /data/blastdb/${build} -i ${fafile}  -p 100 -a 4 -D 3 -e 1e-10 -F F  > /tmp/testblast.txt
    eval megablast -d /data/blastdb/${build} -i ${fafile}  -W 20 -a 4 -D 3 -e 1e-10 -F F  > /tmp/testblast2.txt
    fno=$(tail -n +2 ${ifile} | wc -l)
## tricky with the > 56  but it works
    bno=$(perl -lane 'print if $F[6] == 1 and $F[7] == $F[3]and $F[3] > 56' /tmp/testblast.txt | cut -f 1 | sort | uniq -c | sort -nk2 |wc -l)
    echo ${fno}
    echo ${bno}
    if [ ${bno} == ${fno} ]; then
	paste <(perl -lane 'print join("\t",@F[0..7],$F[8]-1 ,$F[9]) if $F[6] == 1 and $F[7] == $F[3] and $F[3] > 56' /tmp/testblast.txt | sort -k1,1n | cut -f 2,9-10) <(tail -n +2 ${ifile} | cut -f 2)  <(perl -lane 'print $F[0] unless /^#/ ' /tmp/testblast2.txt | sort | uniq -c | sort -nk2 | cut -b -7 | perl -lne '$_ =~ s/\s+//; print') | gzip > ${obed}
	else 
	echo "${ifile} has ${fno} unique sequences but  blast gives ${bno}"
	exit;
    fi
    # if [ ${ifile} == ${fhg19} ]; then
    # 	echo "human hg19 hg18 conversion"
    # 	obedhg18=$(echo ${obed} | sed -e 's/HG19/HG18/g' | sed 's/\.gz$//g' )
    # 	ounmap=$(echo ${obed} | sed -e 's/\.bed.gz/\_unmapped.txt/' )
    # 	liftOver ${obed} ~/data/liftOver/hg19ToHg18.over.chain ${obedhg18} ${ounmap}
    # fi
#tail -n +2 ${
done

list=`find . -maxdepth 1 -type f -regex ".*bed\(\.gz\)?$" | sort`
echo -e "file\tNoProbes\tOverlapProbesFullArray\tChr\tStart\tEnd\tRange\tMinProbeLength\tMaxProbeLength" > report.csv
for file in ${list} ;
do
    echo ${file}
##    continue
    ofile=$(echo ${file} | sed -e 's/^\.\///;')
#    no=$(zcat ${file} | wc -l | cut -d " " -f 1)
#    se=$(perl -lane 'BEGIN { my %hash; }; $hash{$F[0]}{$F[3]} = @F[1..2]; END { print \n";  foreach my $chr (keys %hash) { print "$chr\n"}}' <(zcat ${file}))
    ffile=$(echo ${file} | sed -e 's/\.\///' | sed -e 's/\.bed\.gz$//' )
    export INFILE=$ffile;
    ## overlap minimal 21
    export OVP=$(intersectBed  -a ${file}  -b ${file}   -wo  | perl -lane ' print unless $F[3] eq $F[8]' | awk '$11>20' | wc -l)
    perl -ane 'BEGIN { my %hash; }; $hash{$F[0]}{$F[3]} = [@F[1..2]]; END {foreach my $chr (sort keys %hash) {my $nopr; my $start=9999999999; my $end; $minp=99999999; $maxp; foreach my $id (keys $hash{$chr}) {++$nopr; $start =  $hash{$chr}{$id}[0] if  $hash{$chr}{$id}[0] < $start; $end =  $hash{$chr}{$id}[1] if  $hash{$chr}{$id}[1] >  $end; $minp =  $hash{$chr}{$id}[1] - $hash{$chr}{$id}[0] if ($hash{$chr}{$id}[1] - $hash{$chr}{$id}[0]) < $minp; $maxp =  $hash{$chr}{$id}[1] - $hash{$chr}{$id}[0] if ($hash{$chr}{$id}[1] - $hash{$chr}{$id}[0]) > $maxp };$chr =~ s/^chr//;print join("\t",$ENV{INFILE},$nopr,$ENV{OVP},$chr,$start + 1,$end, ($end-$start),$minp,$maxp),"\n"}} ' <(zcat ${file}) >> report.csv
#    se=$(perl -lane '$min = $F[1] if $min ==0  or $F[1] < $min; $min = $F[2] if $min ==0  or $F[2] < $min; $max = $F[2] if $F[2] > $max; $max = $F[1] if $F[1] > $max; $minps = abs($F[2] -$F[1]) +1 if $minps ==0  or abs($F[2] -$F[1]) +1 < $minps; $maxps = abs($F[2] -$F[1]) if abs($F[2] -$F[1]) > $maxps;  END {print join("\t",$min,$max,$max-$min+1,$minps,$maxps)}' <(zcat ${file}))
done

cat report.csv | perl -lane 'if ($. == 1 ) { print join("\t",@F[0..2],"ProbesWithRE1",@F[3..$#F],"start_RE1","end_RE1")} else { my $org = ($F[0] =~ /_HG1[89]_/) ? "hsapiens" : "mmusculus"; my $build = ($F[0] =~ /_HG1[89]_/) ? ($F[0] =~ /_HG19_/) ? "hg19" : "hg18"  : "mm9"; my $re1 = ($F[0] =~ /_HindIII_/) ? "HindIII" : "BglII"; $reseq = ($F[0] =~ /_HindIII_/) ? "AAGCTT" : "AGATCT"; my $line = qq(grep -P $reseq $F[0]_seq.fa | grep -v -P "^$reseq|$reseq\$" | wc -l); my $probw = qx($line);chomp($probw);
  my $dat = "/data/genomes/$org/$build/enzdb/$build"."_$re1"."2"."$re1.bed.gz";
 my $line = qq(echo "chr$F[3]\t$F[4]\t$F[4]" | intersectBed -a stdin -b $dat -wo | cut -f 5);my $start_RE1 = qx($line); ++$start_RE1;
 my $line = qq(echo "chr$F[3]\t$F[5]\t$F[5]" | intersectBed -a stdin -b $dat -wo | cut -f 6);my $end_RE1 = qx($line);chomp($end_RE1); 
print join("\t",@F[0..2],$probw,@F[3..$#F],$start_RE1,$end_RE1)} ' > report2.csv
mv report2.csv report.csv

#perl -le 'print(join("\t","chr11",1110647,1110647))' | intersectBed -a stdin -b /data/genomes/hsapiens/hg18/enzdb/hg18_BglII2BglII.bed.gz -wo

#cat report.csv | perl -lane 'if ($. == 1 ) { print join("\t",@F[0..2],"ProbesWith",@F[3..$#F],"start_RE1","end_RE1")} else { my $org = ($F[0] =~ /_HG1[89]_/) ? "hsapiens" : "mmusculus"; my $re1 = ($F[0] =~ /_HindIII_/) ? "HindIII" : "BglII"; $reseq = ($F[0] =~ /_HindIII_/) ? "AAGCTT" : "AGATCT"; my $probW = qx( "grep -P $re1 $F[0]_seq.fa | grep -v -P \'(^(>|$re1))|$re1$\' | wc -l");print join("\t",@F[0..2],$probw,@F[3..$#F],"start_RE1","end_RE1") '  #> report2.csv


#echo " show probes cut inside with HindIII"
#grep -v '^>' OID401*.fa | grep AAGCTT | grep -v -P ':AAGCTT|AAGCTT$' 
## do correct with zero based bed
## and
#intersectBed -wb -a 120127_MM9_HindIII_TK_cap_probe_sequences_rep.bed -b /data/THiC/sites/mm9_AAGCTT_CATG.bed.gz | cut -f 7-9 > 120127_MM9_HindIII_TK_cap_probe_sequences_rep_HN.bed
##
#intersectBed -wb -a 120131_HG18_BglII_TK_cap_probe_sequences_rep.bed -b /data/THiC/sites/hg18_AGATCT_CATG.bed.gz | cut -f 6-8 > 120131_HG18_BglII_TK_cap_probe_sequences_rep_BN.bed

popd