use feature ':5.10';

sub dir_one_fw_slash {
    my $dir = shift;
    $dir = $$dir if (ref($dir));
    $dir .= '/';
    $dir  =~ s/\/+/\//g; 
    return($dir);
}

sub rmchr {
    my $chr = shift;
    $chr =~ s/^chr//;
    retrun($chr);
}

# sub chrorder{
#     my @chr = @_;
#     my $c = 0;
#     if ($chr[0] =~ /^chr/ ) {
# 	$c = 1;
# 	map { s/^chr//;} @chr;
#     }
#     # split array in random hap and both
#     my grep { /_(randompattern/ }  @chr;
# ## first random and hap
# ### then random and hap

#     ## remove numeric

#     # find ${dir} -regex ".*\/chr[0-9]_.*random\.fa" -print0 | sort -z | xargs -0 -r cat >> ${file}
#     # find ${dir} -regex ".*\/chr[0-9][0-9]_.*random\.fa" -print0 | sort -z | xargs -0 -r cat >> ${file}
#     # find ${dir} -regex ".*\/chr[XY]_.*random\.fa" -print0 | sort -z | xargs -0 -r cat >> ${file}
#     # find ${dir} -regex ".*\/chrM_.*random\.fa" -print0 | sort -z | xargs -0 -r cat >> ${file}
#     # find ${dir} -regex ".*\/chrUn_.*.fa" -print0 | sort -z | xargs -0 -r cat >> ${file}
#     @chr = map { "chr".$_;} @chr;
#     return(@chr);
# }


sub system_call {
    my $command = join(" ",@_);
## http://www.perlhowto.com/executing_external_commands
#The return value is set in $?; this value is the exit status of the command as returned by the 'wait' call; to get the real exit status of the command you have to shift right by 8 the value of $? ($? >> 8).
#If the value of $? is -1, then the command failed to execute, in that case you may check the value of $! for the reason of the failure.    
    system ($command);
    if ($? == -1) {
	print "system call in $0 with $command failed to execute: $!\n";
	exit(1);
    }
    elsif ($? & 127) {
     	printf "system call in $0 with $command child died with signal %d, %s coredump\n",
     	    ($? & 127),  ($? & 128) ? 'with' : 'without';
     	exit(1);
    }
    else {
	unless($? >> 8 == 0) {
	    printf "system call in $0 with $command child exited with value %d\n", $? >> 8;
	    exit(1);
	} else {
	    ## fine system call
	}
    }
}
1;
