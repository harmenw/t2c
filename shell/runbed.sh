#!/bin/bash
#ibam=/media/bigmama/lala/data/THiC/all_new/old_PE/Beta-globin_FL_trim_HindIII_enzdb_HindIII_NlaIII_PE.bam
dirstart=/media/bigmama/lala/data/THiC/all_new/
odirstart=/data/THiC/harmen_analyis/
mkdir -p ${odirstart}
pushd /data/THiC/sites/
file=hg18_BglII2BglII.bed.gz
if [ ! -f ${file} ]; then 
    zcat /data/THiC/sites/hg18_AGATCT.gz|  tail -n +2 |  cut -f 2-4 | perl -lane ' print join("\t",$F[0],$F[1]-1,$F[2])' | gzip  > ${file}
#    cat ${file}  | sed 1d | perl -F"\t" -lane '$F[1] += 5; $F[2] -= 6; print join("\t",@F)'  > hg18_BglII2BgllII_min0.bed
fi
file=mm9_HindIII2HindIII.bed.gz
if [ ! -f ${file} ]; then 
    zcat /data/THiC/sites/mm9_AAGCTT.gz|  tail -n +2 |  cut -f 2-4 | perl -lane ' print join("\t",$F[0],$F[1]-1,$F[2])' | gzip  > ${file}
fi
popd


for build in mm9 hg18
do 
    for dirp in sam_PE_uniq_whole_genome sam_PE
    do
	idir=${dirstart}${build}/output/${dirp}/
	odir=${odirstart}/${build}/output/${dirp}/matrix/
	odirrep=${odirstart}/${build}/output/${dirp}/matrix_rep/
	mkdir -p ${odir}
	mkdir -p ${odirrep}
	echo ${odir}
	echo ${odirrep}
	bamfiles=( $(find ${idir} -type f -name "*trim*.bam"))
	for (( i = 0; i < ${#bamfiles[@]}; i = i + 1 ))
#	for (( i = 0; i ==0; i = i + 1 ))
	do
### get results 
## cat Beta-globin_FB_II_trim_HindIII_PE_RE1_mat.txt | perl -F"\t" -lanE 'chomp;say if $F[0] eq "chr7" and $F[3] eq "chr7" and $F[1] >= 109875612 and $F[2] <= 109876412 and $F[4] >=  109875612 and $F[5] <= 109876412'
## make nice shell script to compare 
##
	    ibam=${bamfiles[$i]};
	    fname=$(basename ${ibam} | sed -e 's/\.bam//')
	    echo ${fname}
	    if [[ ! -s ${odir}$fname.bed ]];
	    then
		echo "read bam: ${ibam}"
		#chr_1 start_1 end_1 chr_2 start_2 end_2 tag_name quality_score strand1 strand2 reading_primer
 		~/projects/general/perl/bam2bedPE.pl -i <(samtools sort -no ${ibam} -)  > ${odir}$fname.bed
	    fi
	    echo ${odir}$fname.bed
            ## generates double
	    #chr_1 start_1 end_1 chr_2 start_2 end_2 dummy strand1 strand2 reading_primer
   	    cat ${odir}${fname}.bed  | cut -f 1-6,9-11 | sort -k 1,1 -k 2,2n | uniq -c | perl -F"\t" -lanE '$F[0] =~ s/^\s+//; my @first = split(" ",$F[0]); say join("\t", $first[1], @F[1..5],"-",$first[0],@F[6..8]);' > ${odir}${fname}_2.bed
	    #chr_1 start_1 end_1 chr_2 start_2 end_2 dummy strand1 strand2 reading_primer
	    ## adding up same reads from different primers
	    cat ${odir}${fname}_2.bed | cut -f -10 |perl -F"\t" -laE 'while (<STDIN>) {if ($. == 1) {chomp;@line1 = split("\t",$_);next;}; chomp;my @line2 =split("\t"); if( @line1[1..6] ~~ @line2[1..6]) {$line1[7] += $line2[7]} else {say join "\t",@line1; @line1=@line2}}; say join "\t",@line1' > ${odir}${fname}_read_mat.bed

# ##  set reads to restriction bins
# ## -f 90% overlap ( sometime not trimmed properly)(reading error some extra bases; both sites in sites
 	    if [[ $build == "mm9" ]]; then 
		REbed=/data/THiC/sites/mm9_HindIII2HindIII.bed.gz
		probeRep=/data/THiC/Oligos_on_the_array/120127_MM9_HindIII_TK_cap_probe_sequences_rep_HN.bed
	    else
		REbed=/data/THiC/sites/hg18_BglII2BglII.bed.gz
		probeRep=/data/THiC/Oligos_on_the_array/120131_HG18_BglII_TK_cap_probe_sequences_rep_BN.bed
	    fi
### remove reads that cover repeat probes for 100 %
	    # read should overlap HindIII site completly otherwise the read is trimmed over HindIII site 
	    #chr_1 start_1 end_1 chr_2 start_2 end_2 dummy strand1 strand2 chr_site site_start site_end
 	    pairToBed -a ${odir}${fname}_read_mat.bed -b ${REbed} -f 1 -type both > ${odir}${fname}_read2RE1.txt
	    ## check equal
            # cat ${odir}${fname}_read2RE1.txt |perl -F"\t" -lane 'if ($. % 2 == 1) {@old = @F[0..9] } else { print $. unless @old ~~ @F[0..9]}'
	    ##
# ## make RE1 -Re1 interaction
## harmen
## maybe not the best for non-cut since -+ -+ or also removed if both are inverted
## harmen
 	    perl -F"\t" -l11 -anE 'chomp;print join("\t", @F[10..12]); say join("\t",@F[7..9]) if $. % 2 == 0;'  ${odir}${fname}_read2RE1.txt | perl -F"\t" -lanE '$sw=""; $sw = "s" if @F[1..2] ~~ @F[4..5] and ($F[7] ne $F[8]); $sw = "v" if @F[1..2] ~~ @F[4..5] and ($F[7] eq $F[8]); $sw = "n" if $F[2] -6 == $F[4] and $F[7] eq "+" and  $F[8] eq "-"; say join("\t", @F,$sw)' > ${odir}${fname}_RE12RE1.txt
# ## merge multiple RE1RE2 from different read coordinates
 	    cat ${odir}${fname}_RE12RE1.txt | perl -F"\t" -lane '++$F[1]; ++$F[4]; $hash{join("\t",@F[0..5])} += $F[6]; END {foreach my $key (sort keys %hash) { print $key,"\t",$hash{$key}} }' > ${odir}${fname}_RE1_mat.txt
 	    cat ${odir}${fname}_RE12RE1.txt | perl -F"\t" -lane '++$F[1]; ++$F[4]; $hash{join("\t",@F[0..5])} += $F[6] unless $F[9]; END {foreach my $key (sort keys %hash) { print $key,"\t",$hash{$key}} }' > ${odir}${fname}_RE1_nons_mat.txt
	    ## last two can be merged inn sibgle command
##head -100 Beta-globin_FB_II_trim_HindIII_PE_read2RE1.txt | perl -F"\t" -l11 -anE 'chomp;print join("\t", @F[8..10]); say $F[7] if $. % 2 == 0;' | sort -k1,1 -nk2,2 -nk3,3 -k 4,4 -nk 5,5 -nk 6,6 | perl -F"\t" -lane '$hash{join("\t",@F[0..5])} += $F[6]; END {foreach my $key (sort keys %hash) { print $key,"\t",$hash{$key}} }' 
	    ## same
            ## with  repeat
	    ## 
## not on alignment but on RE1-RE2 site of rep
# 	    pairToBed -a ${odir}${fname}_read_mat.bed  -b ${probeRep} -f 20E-9 -type neither > ${odirrep}${fname}_read_mat_norep.bed
# ##
# 	    ## 
#  	    pairToBed -a ${odirrep}${fname}_read_mat_norep.bed -b ${REbed} -f 1 -type both > ${odirrep}${fname}_read2RE1_norep.txt
#  	    perl -F"\t" -l11 -anE 'chomp;print join("\t", @F[10..12]); say join("\t",@F[7..9]) if $. % 2 == 0;'  ${odirrep}${fname}_read2RE1_norep.txt | perl -F"\t" -lanE '$sw=""; $sw = "s" if @F[1..2] ~~ @F[4..5] and ($F[7] ne $F[8]); $sw = "v" if @F[1..2] ~~ @F[4..5] and ($F[7] eq $F[8]); $sw = "n" if $F[2] -5 == $F[4] and $F[7] eq "+" and  $F[8] eq "-"; say join("\t", @F,$sw)' > ${odirrep}${fname}_RE12RE1_norep.txt
#  	    cat ${odirrep}${fname}_RE12RE1_norep.txt | perl -F"\t" -lane '$hash{join("\t",@F[0..5])} += $F[6]; END {foreach my $key (sort keys %hash) { print $key,"\t",$hash{$key}} }' > ${odirrep}${fname}_RE1_mat_norep.txt
#  	    cat ${odirrep}${fname}_RE12RE1_norep.txt | perl -F"\t" -lane '$hash{join("\t",@F[0..5])} += $F[6] unless $F[9]; END {foreach my $key (sort keys %hash) { print $key,"\t",$hash{$key}} }' > ${odirrep}${fname}_RE1_nons_mat_norep.txt
	done;
    done;
done;
exit
# ## not head but once can be run last two staements
# head -100 Beta-globin_FB_II_trim_HindIII_PE_read2RE1.txt | perl -F"\t" -l11 -anE 'chomp;print join("\t", @F[8..10]); say $F[7] if $. % 2 == 0;' | sort -k1,1 -nk2,2 -nk3,3 -k 4,4 -nk 5,5 -nk 6,6 | perl -F"\t" -lane '$hash{join("\t",@F[0..5])} += $F[6]; END {foreach my $key (sort keys %hash) { print $key,"\t",$hash{$key}} }' > ${odir}${fname}_RE1_mat.txt

# ### find 

# # samtools view -h  ${ibam} | perl -F"\t" -lanE ' unless ($F[0] =~ /^@/) {$F[0] =~ s/(#0\/)(1|3):[01]//;} print join("\t",@F);' | samtools view -uhbS - | samtools sort - -n /tmp/test

# # #samtools view -h  ~/projects/THiC/data/testset/mm9/output/sam_PE/Beta-globin_FL_test_trim_HindIII_enzdb_HindIII_NlaIII_PE.bam | perl -F"\t" -lanE ' unless ($F[0] =~ /^@/) {$F[0] =~ s/(#0\/)(1|3):[01]/#0\/$2/;} print join("\t",@F);' | samtools view -uhbS - | samtools sort - -n /tmp/test
# # #
# # bamToBed -bedpe -i /tmp/test.bam  > /tmp/testPE.bed
# # ## +-
# # echo "total mol. pairs"
# # wc -l /tmp/testPE.bed
# # echo "+-"
# # cut -f 9- /tmp/testPE.bed  | sort | uniq -c 
# # echo "unique pairs"
# # cut -f -6 /tmp/testPE.bed  | sort | uniq -c | wc -l


# ## harmen bed5B
# echo "read bam"
# ~/projects/general/perl/bam2bedPE.pl -i <(samtools sort -no ${ibam} -)  > /tmp/harmenPE.bed

# #echo "+-"
# #cut -f 9-10 /tmp/harmenPE.bed  | sort | uniq -c 

# #echo "total mol. pairs"
# #wc -l /tmp/harmenPE.bed
# #
# cat /tmp/harmenPE.bed  | cut -f 1-6,9-11 | sort -k 1,1 -k 2,2n | uniq -c | perl -F"\t" -lanE '$F[0] =~ s/^\s+//; my @first = split(" ",$F[0]); say join("\t", $first[1], @F[1..5],"-",$first[0],@F[6..8]);' > /tmp/harmenPE2.bed

# #echo "unique molecules"
# #wc -l /tmp/harmenPE2.bed

# #echo "uniq  mol +-"
# #cut -f 9-10 /tmp/harmenPE2.bed  | sort | uniq -c 

# ## bedtools full overlap a single hindIII fragment
# pushd /data/THiC/sites/
# file=hg18_BglII2BgllII.bed
# if [ ! -f ${file} ]; then 
#     zcat hg18_AGATCT.gz | sed 1d | cut -f 2-4  > ${file}
# fi
# file=mm9_HindIII2HindIII.bed
# if [ ! -f ${file} ]; then 
#     zcat mm9_AAGCTT.gz  | sed 1d | cut -f 2-4  > ${file}
# fi
# popd

# ## merge  fw-rev rev_ into a single row by addiong up  reads 
# cat /tmp/harmenPE2.bed | cut -f -8 |perl -F"\t" -laE 'while (<STDIN>) {if ($. == 1) {chomp;@line1 = split("\t",$_);next;}; chomp;my @line2 =split("\t"); if( @line1[1..6] ~~ @line2[1..6]) {$line1[7] += $line2[7]} else {say join "\t",@line1; @line1=@line2}}; say join "\t",@line1' > /tmp/harmenPE2_tobias_mat.txt
# ##  set reads to restriction bins
# ## -f 90% overlap ( sometime not trimmed properly)(reading error some extra bases; both sites in sites
# pairToBed -a /tmp/harmenPE2_tobias_mat.txt -b /data/THiC/sites/mm9_HindIII2HindIII.bed  -f 1 -type both > /tmp/harmenPE2_read2RE1sites.txt
# ## make RE1 -Re1 interaction
# perl -F"\t" -l11 -anE 'chomp;print join("\t", @F[8..10]); say $F[7] if $. % 2 == 0;'    /tmp/harmenPE2_read2RE1sites.txt  |sort > /tmp/harmenPE2_RE12RE1.txt

# ## merge mulirple RE1RE2 from different read coordinates
# cat /tmp/harmenPE2_RE12RE1.txt |perl -F"\t" -laE 'while (<STDIN>) {if ($. == 1) {chomp;@line1 = split("\t");next;}; chomp;my @line2 =split("\t"); if( @line1[1..5] ~~ @line2[1..5]) {$line1[6] += $line2[6]} else {say join "\t",@line1; @line1=@line2}}; say join "\t",@line1' > /tmp/Beta-globin_FL_trim_HindIII_enzdb_HindIII_NlaIII_PE_mat.txt
