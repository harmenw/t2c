#!/usr/bin/perl
use strict;
use warnings;

# ~/projects/THiC/perl/digestion_RE_genome.pl -i /data/genomes/mmusculus/mm9/merge/mm9_merge.fa -o /data/genomes/mmusculus/mm9/enzdb/ -re1 HindIII -re2 NlaIII -v

### harmen to do
### 1) play with - f 1 in uniq genome
### altough what if over a HindII which is not detected(Snp?)
### 2) reads shoul be in on the proper strand N------------H-----------N should be towards H and not toeards N !! but be careful with reads from hind site 
### 3) look at edit distnace NM:I:5 for instance in enzdb
### 4) reads which are not on top of restriction sites give overvioew
##  5) note tha now n--------H---n-----H--------n is one site since only one NlaIII site between HindIII sites
### 6) later maybe anlign only on fragment HindIII-NlaIII is better2 but with hinhind you might lose !! think about it 
##
###
##  sampe slow on trim trim length of reads!!!!!!!!!!!!!!
#################
# compare to rutger

require v5.10;
use feature qw(say switch);

use Getopt::Long qw(:config no_ignore_case);
use File::Find::Rule;
use File::Basename;
use File::Copy;
use Parallel::ForkManager;
use Switch;
use Bio::Restriction::EnzymeCollection;
use File::Path qw(remove_tree);

our $VERSION = "0.1_001";
$VERSION = eval $VERSION;

###
my $dir;
my $bwa_index;
my $enz_bwa;
my $br1;
my $r2r;
my $c;
my $f = 1; # max. number forks
my $t = 1;
my $aln;
my $m = 5e8; # sort memory
my $r;
my $q;
my $p;
my $F; #force
my $v; #verbose
my $V; #Version
my $h; #help

GetOptions(
    "f|fork:i"=>\$f,
    "F|force"=>\$F, # flag
    "br1|bedr1=s"=>\$br1,
    "r2r=s"=>\$r2r,
    "c|qc|qualitycontrol"=>\$c,
    "e|enz|enzbwa=s"=>\$enz_bwa,
    "p|pe|paired-end"=>\$p,
    "q|qqpoor"=>\$q,
    "a|aln"=>\$aln,
    "b|bwa_index=s"=>\$bwa_index,
    "d|directory|path=s"=>\$dir,
    "r|re=s"=>\$r,
    "m|sortmemory:i"=>\$m,
    "t|threads:i"=>\$t,
    "v|verbose"=>\$v, # flag
    "V|version"=>\$V, # flag
    "h|help|?" => \$h);  

if ($V) {
        print "\n$0 v$VERSION\n\n";
        exit 0;
        }
usage() if $h;

my $time;
if ($v) { use Time::Elapsed qw(elapsed);
        $time =time()};

usage() unless defined($dir);
usage() unless defined($bwa_index);
usage() unless defined($enz_bwa);
usage() unless defined($r);
usage() unless defined($br1);
usage() unless defined($r2r);

say "### $0 started: ". localtime() . " ###" if $v;
$dir .= '/';
$dir =~ s/\/+/\//;
say $dir;

unless (-d $dir) {
    die "$0 $dir does not exists or is not a directory\n";
}

unless (-e "$bwa_index.pac" and -s "$bwa_index.pac" ) {
    die "$0 bwa index $bwa_index does not exists or is empty\n";
}

unless (-e $br1 ) {
    die "$0 no bed file found: $br1 \n";
}

unless (-e $r2r ) {
    die "$0 no bed file found: $r2r \n";
}

my $pm = Parallel::ForkManager->new($f) if $f > 1;
my $enz_col = Bio::Restriction::EnzymeCollection->new;
my $enz = $enz_col->get_enzyme($r);
die "Unknown restriction enzyme $r\n" unless defined $enz;
my $enz_site = $enz->seq->seq();
my $enz_reexp = $enz->recog;
say "$r: $enz_site Regular expression $enz_reexp" if $v;
## get fastq files

my $odir = "$dir"."output";
mkdir $odir, +0755 or die "Directory $odir cannot be created: $!\n" unless -d  $odir;
## mkdir output/stats


my @fqf = fqfindir($dir);

### make gz from fasta
foreach my $fqf (sort @fqf) {
   if ($f > 1) { $pm->start and next} #start fork
    if ($fqf =~ /\.f(ast)?q$/) { ## not gzipped
	say "zipped $fqf" if $v;
	system_call("gzip -f $fqf");
	$fqf .= '.gz';
    }
    $pm->finish if $f > 1;	
}
$pm->wait_all_children if $f > 1;

### make re1 trim 

@fqf = fqfindir($dir);
foreach my $fqf (sort @fqf) {
    next if $fqf =~ /_(36b|$r|qc_)/;
    if ($f > 1) { $pm->start and next} #start fork
    say $fqf if $v;
    my $cat = detcat($fqf);
    my $fqf_re = fqf_trim($fqf,"trim_$r");
    if ( !-e $fqf_re or -z $fqf_re or $F or ! comp2files($fqf, $fqf_re)) {
	system_call("$cat $fqf | perl -nE 'chomp; if (\$. % 4 == 2 ) { \$_ =~ /$enz_reexp/; \$pos = scalar(\@+[0]); \$pos = length unless defined \$pos}; if (\$. % 4 == 2 or \$. % 2 == 0 ) { say substr(\$_,0,\$pos)}  else { say};' | gzip > $fqf_re~");
	move $fqf_re.'~', $fqf_re or die "cannot move $fqf_re\~: $!\n";
    }
    $pm->finish if $f > 1;	
}
$pm->wait_all_children if $f > 1;


@fqf = fqfindir($dir);
###
###
### trimmed quality 
foreach my $fqf2 (sort @fqf) {
    next if $fqf2 =~ /_qc_/;
    next unless $fqf2 =~ /_2(_trim_$r)?\.f(ast)?q(\.gz|\.bz2|\.zip)?$/;
    next unless $fqf2 =~ /_trim_$r/; # reduce number of files
    if ($f > 1) { $pm->start and next} #start fork
    my $cat = detcat($fqf2);
    my $QF = check_quality_format($fqf2,$cat);
    (my $fqf1 = $fqf2) =~ s/_2((_trim_$r)?\.f(ast)?q(\.gz|\.bz2|\.zip)?)$/_1$1/;
    (my $fqf1_qc = $fqf1) =~ s/(_1(_trim_$r)?\.f(ast)?q(\.gz|\.bz2|\.zip)?)$/_qc$1/;
    my $fqf1_unp = $fqf1_qc.".unpaired.gz";
    (my $fqf2_qc = $fqf2) =~ s/(_2(_trim_$r)?\.f(ast)?q(\.gz|\.bz2|\.zip)?)$/_qc$1/;
    my $fqf2_unp = $fqf2_qc.".unpaired.gz";
    if ( !-e $fqf1_qc or !-e $fqf2_qc  or -z $fqf1_qc or -z $fqf2_qc or $F or ! comp2files($fqf1, $fqf1_qc) or ! comp2files($fqf2, $fqf2_qc)) {
	##minimimu length 13 check empirically but maybe lower!
	system_call("TrimmomaticPE -phred$QF -threads ${t} $fqf1 $fqf2 $fqf1_qc $fqf1_unp $fqf2_qc $fqf2_unp LEADING:10 TRAILING:10 MINLEN:12");
	#singletons kept
	# later remove?
    }
    $pm->finish if $f > 1;	
}
$pm->wait_all_children if $f > 1;

## make fastqc
@fqf = fqfindir($dir);
foreach my $fqf (sort @fqf) {
    next unless $fqf =~ /_(qc_.*_trim_|[12]\.fastq\.gz)/;
    next if $fqf =~ /_(36b)/;
    if ($f > 1) { $pm->start and next} #start fork
    say $fqf if $v;
    my $cat = detcat($fqf);
    ## -Q 64 default Solexa quals
    ## -Q 33 sanger quals
    my $QF = check_quality_format($fqf,$cat);
    ### make quality
## start QC
    ## mkdir output
    my $stats = $odir."/stats";
    mkdir $stats, +0711 or die "Directory $stats cannot be created: $!\n" unless -d  $stats;
    my $bname = basename($fqf);
    my ($expname) = (split /\./, $bname)[0];
    my $ofile = "$stats/$expname"."_fastqc.zip";
    my $prog = "fastqc";
    if ( !-e $ofile or -z $ofile or $F or ! comp2files($fqf, $ofile)) {
	say $ofile if $v;
	system_call ("$prog $fqf --outdir $stats");
    }
    if ($c) { 
	$pm->finish if $f > 1;	
	next;
    }
    $pm->finish if $f > 1;	
}
$pm->wait_all_children if $f > 1;


## single end
foreach my $fqf (sort @fqf) {
    next unless $fqf =~ /_qc_.*_trim_/;
##    next unless $fqf =~ /.*_trim_/;
    next if $fqf =~ /_(36b)/;
    if ($f > 1) { $pm->start and next} #start fork
    say $fqf if $v;
#    my $fqf_re = fqf_trim($fqf,"trim_$r");
    ## make restriction site
    ## mkdir output
    ## mkdir output/stats
    my $bwa = $odir."/bwa";
    mkdir $bwa, +0711 or die "Directory $bwa cannot be created: $!\n" unless -d  $bwa;
#    foreach my $ifqf ($fqf, $fqf_36, $fqf_re) {
#    foreach my $ifqf ($fqf, $fqf_re) {
    foreach my $ifqf ($fqf) {
	## on full genome and restricted genome
	foreach my $i (1,2) {
	    my $ofile = $bwa .  '/' .  basename($ifqf);
	    if ($i == 1) {
		$ofile = fsam($ofile,"sai");
	    } else {
		$ofile = fsam($ofile,"sai","",$enz_bwa);
	    }
	#index
	    if ( !-e $ofile or -z $ofile or $F or ! comp2files($ifqf, $ofile)) {
		say $ofile if $v;
		my $bwa_attr = "";
		$bwa_attr = "-q 20" if $q;
		if ($i == 1) {
		    system_call("bwa aln -t $t $bwa_attr $bwa_index $ifqf > $ofile\~");
		} else {
		    system_call("bwa aln -t $t $bwa_attr $enz_bwa $ifqf > $ofile\~");
		}
		move $ofile.'~', $ofile or die "cannot move $ofile\~: $!\n";
	    }
	    $pm->finish if $f > 1 and $aln;
	    next if $aln;
	    my $sam = $odir."/sam";
	    mkdir $sam, +0711 or die "Directory $sam cannot be created: $!\n" unless -d  $sam;
	    my $osam =  $sam .  '/' .  basename($ifqf);
	    if ($i == 1) {
		$osam = fsam($osam,"sam");
	    } else {
		$osam = fsam($osam,"sam","",$enz_bwa);
	    }
	    my $opbam =  $sam .  '/' .  basename($ifqf);
	    if ($i == 1) {
		$opbam = fsam($opbam,"pbam");
	    } else {
		$opbam = fsam($opbam,"pbam","",$enz_bwa);
	    }
	    my $obai = "$opbam.bam.bai";
#	    (my $obai_pe = $osam) =~ s/_[12]((_36b|_trim_$r)?(_(.*?)_(.*?)\.sam$)/$1/;
#	    $obai_pe =~  s/\.sam$/_PE\.bam\.bai/;
#	    say $obai_pe if $v;
	    if ( !-e $obai or -z $obai or $F or ! comp2files($ofile, $obai)) {
		say $opbam if $v;
		if ($i == 1) {
		    system_call(qq(bwa samse $bwa_index $ofile $ifqf| grep ).q('^@\|\sXT:A:U\s').qq( | samtools view -ubS - | samtools sort -m $m - $opbam~)); 
		} else {
		    system_call(qq(bwa samse $enz_bwa $ofile $ifqf| grep ) .q('^@\|\sXT:A:U\s') .qq( | samtools view -ubS - | samtools sort -m $m - $opbam~)); }
		move $opbam.'~.bam', $opbam.'.bam' or die "cannot move $opbam\~: $!\n";
		say $obai if $v;
		system_call("samtools index $opbam.bam");
	    ## also remove sai's?
#		unlink $osam or die "cannot remove $osam: $!\n";
	    # if ($osam =~ /_2(_36b|_trim_$r)?\.sam/) {
	    # 	(my $ibam2 = $osam) =~ s/\.sam$/\.bam/;
	    # 	(my $ibam1 = $ibam2) =~ s/_2((_36b|_trim_$r)?\.bam$)/_1$1/;
	    # 	(my $obamsm = $ibam2) =~ s/_2((_36b|_trim_$r)?\.bam$)/$1/;
	    # 	$obamsm =~ s/\.bam/_PE/;
	    # 	my $obam = $obamsm . '.bam';
	    # 	system_call(qq(samtools merge - $ibam1 $ibam2 ) .q( | samtools sort - -no - | samtools view -h - | perl -nE 'chomp;if (/^\@SQ/) {say;next};push(@line,$_); if ($#line == 1) {@one = (split(/#/, $line[0]))[0..1]; $two = (split(/#/, $line[1]))[0]; if ($one[0] eq $two) {@line = reverse(@line) unless $one[1] =~ /^0\/1/; say join("\n",@line); @line = ();} else { my $foo = shift @line}}' | samtools view -ubS - | ) .qq(samtools sort - $obamsm));
	    # 	system_call("samtools index $obam");
	    # 	(my $ofl_pe = $obam) =~ s/\.bam$/\.flstat/;
	    # 	if ( !-e $ofl_pe or -z $ofl_pe or $F or ! comp2files($obai_pe, $ofl_pe)) {
	    # 	    say $ofl_pe if $v;
	    # 	    system_call("samtools flagstat $obam > $ofl_pe\~");
	    # 	    move $ofl_pe.'~', $ofl_pe or die "cannot move $ofl_pe\~: $!\n";
	    # 	}
	    # }
		my $ofl =  $sam .  '/' .  basename($ifqf);
		if ($i == 1) {
		    $ofl = fsam($ofl,"flstat");
		} else {
		    $ofl = fsam($ofl,"flstat","",$enz_bwa);
		}
		if ( !-e $ofl or -z $ofl or $F or ! comp2files($obai, $ofl)) {
		    say $ofl if $v;
		    system_call("samtools flagstat $opbam.bam > $ofl\~");
		    move $ofl.'~', $ofl or die "cannot move $ofl\~: $!\n";
		}
	    }
	}
    }
    $pm->finish if $f > 1;	
}
$pm->wait_all_children if $f > 1;
exit if $aln or $c;

# paired_end
foreach my $fqf (sort @fqf) {
    next unless $fqf =~ /_qc_2_trim_/;
    next if $fqf =~ /_(36b)/;
    if ($f > 1) { $pm->start and next} #start fork
    my $cat = detcat($fqf);
#    my $fqf_36 = fqf_trim($fqf,"36b");
#    my $fqf_re = fqf_trim($fqf,"trim_$r");
    ## make restriction site
    ## mkdir output
    my $odir = "$dir"."output";
    ## mkdir output/stats
    my $bname = basename($fqf);
    my ($expname) = (split /\./, $bname)[0];
    my $bwa = $odir."/bwa";
    foreach my $ifqf ($fqf) {#, $fqf_re) {
	## on full genome and restricted genome
	foreach my $i (1,2) {
	    my $sam_PE = $odir."/sam_PE";
#	$sam .= "_PE" if $p;
	    mkdir $sam_PE, +0711 or die "Directory $sam_PE cannot be created: $!\n" unless -d  $sam_PE;
	    my $bwa =  $odir . "/bwa" .  '/' .  basename($ifqf);
	    my $osam =  $odir . "/sam" .  '/' .  basename($ifqf);
	    my $osam_PE =  $sam_PE .  '/' .  basename($ifqf);
	    my $isai2;
	    my $obamsm;
	    if ($i == 1) {
	     	$isai2 = fsam($bwa,"sai");
	     	$obamsm = fsam($osam_PE,"pbam","PE");
	     	$osam = fsam($osam_PE,"sam","PE");
	    } else {
	     	$isai2 = fsam($bwa,"sai","",$enz_bwa);
	     	$obamsm = fsam($osam_PE,"pbam","PE",$enz_bwa);
	     	$osam = fsam($osam_PE,"sam","PE",$enz_bwa);
	    }
	    (my $isai1 = $isai2) =~  s/(_qc)?_2((_36b|_trim_$r)?(_enzdb_$r\_.*)?\.sai$)/$1_1$2/;
	    (my $ifqf1 = $ifqf) =~ s/(_qc)?_2((_36b|_trim_$r)?\.f(ast)?q(\.gz|\.bz2|\.zip)$)/$1_1$2/;
	    $obamsm =~ s/_2_/_/;
	    my $obam = $obamsm . '.bam';
	    (my $sobamsm = $obam) =~ s/\.bam/\.sort\.bam/;
	    ###
	    my $obai = $obam;
	    $obai .= '.bai';
	    if ( !-e $obai or -z $obai or !-e $obam or -z $obam or $F or ! comp2files($isai1, $obai) or ! comp2files($isai2, $obai) or ! comp2files($isai1, $obam) or ! comp2files($isai2, $obam)) {
		say $obam if $v;
		my $index;
		if ($i == 1) {
		    $index = $bwa_index;
		} else {
		    $index = $enz_bwa;
		}
		my $tmpodir = '.tmp/'.basename($obamsm);
#		system_call("bwa sampe -A -P $index $isai1 $isai2 $ifqf1 $ifqf | samtools view -uhbS - | samtools sort -m $m - -n $obamsm~"); 
#		system_call("bwa sampe -A -P $index $isai1 $isai2 $ifqf1 $ifqf" .qq( | samtools view -uhbS - | samtools sort -m $m - -no - | samtools view -h - | ) . q( perl -nE 'chomp;if (/^\@/) {say;next};next unless /\tXT:A:U\t/;push(@line,$_); if ($#line == 1) {$one = (split(/#|\s/, $line[0]))[0]; $two = (split(/#|\s/, $line[1]))[0]; if ($one eq $two) {say join("\n",@line); @line = ();} else { my $foo = shift @line}}' | samtools view -ubSh - | ) .qq(samtools sort -m $m - $obamsm~)); 
		system_call("bwa sampe -A -P $index $isai1 $isai2 $ifqf1 $ifqf" .qq( | samtools view -uhbS - | samtools sort -m $m - -no $sobamsm | samtools view -h - | ) . q( perl -nE 'chomp;if (/^\@/) {say;next};next unless /\tXT:A:U\t/;push(@line,$_); if ($#line == 1) {$one = (split(/#|\s/, $line[0]))[0]; $two = (split(/#|\s/, $line[1]))[0]; if ($one eq $two) {say join("\n",@line); @line = ();} else { my $foo = shift @line}}' | samtools view -ubSh - | ) .qq(samtools sort -m $m - $obamsm~)); 
		move $obamsm.'~.bam', $obam or die "cannot move $obamsm\~.bam: $!\n";
		system_call("samtools index $obam");
		(my $ofl = $obam) =~ s/\.bam$/\.flstat/;
		if ( !-e $ofl or -z $ofl or $F or ! comp2files($obai, $ofl)) {
		    say $ofl if $v;
		    system_call("samtools flagstat $obam > $ofl\~");
		    move $ofl.'~', $ofl or die "cannot move $ofl\~: $!\n";
		}
	    # if ($osam =~ /_2(_36b|_trim_$r)?\.sam/) {
	    # 	(my $ibam2 = $osam) =~ s/\.sam$/\.bam/;
	    # 	(my $ibam1 = $ibam2) =~ s/_2((_36b|_trim_$r)?\.bam$)/_1$1/;
	    # 	(my $obamsm = $ibam2) =~ s/_2((_36b|_trim_$r)?\.bam$)/$1/;
	    # 	$obamsm =~ s/\.bam/_PE/;
	    # 	my $obam = $obamsm . '.bam';
	    # 	system_call(qq(samtools merge - $ibam1 $ibam2 ) .q( | samtools sort - -no - | samtools view -h - | perl -nE 'chomp;if (/^\@SQ/) {say;next};push(@line,$_); if ($#line == 1) {@one = (split(/#/, $line[0]))[0..1]; $two = (split(/#/, $line[1]))[0]; if ($one[0] eq $two) {@line = reverse(@line) unless $one[1] =~ /^0\/1/; say join("\n",@line); @line = ();} else { my $foo = shift @line}}' | samtools view -ubS - | ) .qq(samtools sort - $obamsm));
	    # 	system_call("samtools index $obam");
	    # 	(my $ofl_pe = $obam) =~ s/\.bam$/\.flstat/;
	    # 	if ( !-e $ofl_pe or -z $ofl_pe or $F or ! comp2files($obai_pe, $ofl_pe)) {
	    # 	    say $ofl_pe if $v;
	    # 	    system_call("samtools flagstat $obam > $ofl_pe\~");
	    # 	    move $ofl_pe.'~', $ofl_pe or die "cannot move $ofl_pe\~: $!\n";
	    # 	}
	    # }
	    }
	    }
    }
    $pm->finish if $f > 1;	
}
$pm->wait_all_children if $f > 1;
# paired end

#paired end uniq whole genome
## only trim via enzdb
my @bam = File::Find::Rule
    ->extras({ follow => 1 }) 
    ->name( qr/.*_enzdb_.*_PE\.*bam$/ )
    ->mindepth( 1 )
#    ->maxdepth( 1 )
    ->in($dir."output/sam_PE/");

foreach my $bam (sort @bam) {
    if ($f > 1) { $pm->start and next} #start fork
    ## get directory
    my  $bdir = dirname($bam) . "_uniq_whole_genome/";
    mkdir $bdir, +0755 or die "Directory $bdir cannot be created: $!\n" unless -d  $bdir;
    #
    my $bfile = basename($bam);
    ## change bam file
    $bfile =~ s/(_enzdb_)/_uniq_genome$1/;
    #change input bamfil e not enzdb but full one 
    (my $nbam= $bam)  =~ s/_enzdb.*_PE\.bam$/_PE\.bam/;
    my $obam = $bdir.$bfile;
    say $obam if $v;
    (my $obamsm = $obam)  =~ s/\.bam//;
    (my $sobamsm = $obam) =~ s/\.bam/\.sort\.bam/;
    if ( !-e $obam or -z $obam or $F or ! comp2files($nbam,$obam)) {
##f hindIII + dpnII is 10 bases maybe later base percentage
	system_call("intersectBed -a $r2r -b $br1  -u | intersectBed -abam $nbam -b stdin  -f 1 " .qq( | samtools sort -m $m - -no $sobamsm | samtools view -h - | ) . q( perl -nE 'chomp;if (/^\@/) {say;next};next unless /\tXT:A:U\t/;push(@line,$_); if ($#line == 1) {$one = (split(/#|\s/, $line[0]))[0]; $two = (split(/#|\s/, $line[1]))[0]; if ($one eq $two) {say join("\n",@line); @line = ();} else { my $foo = shift @line}}' | samtools view -ubSh - | ) .qq(samtools sort -m $m - $obamsm~)); 
	move $obamsm.'~.bam', $obam or die "cannot move $obamsm\~.bam: $!\n";
	system_call("samtools index $obam");
	(my $ofl = $obam) =~ s/\.bam$/\.flstat/;
	if ( !-e $ofl or -z $ofl or $F or ! comp2files($obam.".bai", $ofl)) {
	    say $ofl if $v;
	    system_call("samtools flagstat $obam > $ofl\~");
	    move $ofl.'~', $ofl or die "cannot move $ofl\~: $!\n";
	}
    }
    $pm->finish if $f > 1;	
}
$pm->wait_all_children if $f > 1;


printf "### $0 ended after: %s on: %s ###\n", elapsed(time()-$time), scalar(localtime()),"\n" if $v;
exit ;

sub fqf_trim {
    my $fqf = shift;
    my $method = shift;
    (my $ofqf = $fqf) =~ s/(\.f(ast)?q(\.gz|\.bz2|\.zip))$/_$method$1/;
    return $ofqf;
}

sub fsam {
    my $file = shift;
    my $method = shift;
    my $pe = shift;
    my $enz_bwa = shift; ## enzyme
    die "no method \n" unless defined($method);
    my $r = $file;
    $r =~ s/\.f(ast)?q(\.gz|\.bz2|\.zip)$//;
    if (defined $enz_bwa and $enz_bwa ne "") {
	my $enzf = basename($enz_bwa);
	my @enzf = split('_',$enzf);
	$r .= "_enzdb_".join('_',@enzf[1..2]);
    }
#
    if (defined $pe and $pe ne "") {
	$r =~ s/_[12]$//;
	$r .= "_PE";
    }
#
    switch ($method) {
	case (/sa[im]|flstat/)	{ $r .= "\.$method" }
	case ("pbam")	{ }
	else		{ die "method $method unknown\n"}
    }
    return ($r);
}


sub detcat {
    my $fgf = shift;
    my $r;
    switch ($fgf) {
	case (/\.gz$/)	{ $r = "zcat"}
	case (/\.bz2$/)	{ $r = "bzcat"}
	case (/\.zip$/)	{ $r = "unzip"}
	else		{ $r = "cat"}
    }
    return($r);
}

sub check_quality_format {
    my $fqf = shift;
    my $cat = shift;
    # Read first 100000 quality lines
    chomp(my $asc = qx($cat $fqf |  perl -nE 'if (\$. % 4 == 0) { my \@arr = split//;foreach my \$arr (\@arr) {say ord(\$arr);};} '| head -1000000 | sort -nk2 | uniq -c | head -1 | cut -b 9-));
    my $qvalue = 0;
    if ( $asc < 59 ) {
	$qvalue = 33;
    } else  {
	$qvalue = 64;
    }
    return($qvalue);
}
#y QF = check_quality_format($fgf,$cat);

sub comp2files {
    my $firstf = shift;
    my $secf = shift;
    my @fstat = stat $firstf;
    my @sstat = stat $secf;
    ##mtime 
    if ($fstat[9] <= $sstat[9]) {
	##first is younger 
	return(1);
    } else {
	return(0);
    }
}

sub fqfindir {
    my $sdir = shift;
    my @fqf = File::Find::Rule
	->extras({ follow => 1 }) 
	    ->name( qr/\.f(ast)?q(\.gz|\.bz2|\.zip)?$/ )
		->mindepth( 1 )
		    ->in($sdir);
    return(@fqf);
}



sub system_call {
    my $command = join(" ",@_);
## http://www.perlhowto.com/executing_external_commands
#The return value is set in $?; this value is the exit status of the command as returned by the 'wait' call; to get the real exit status of the command you have to shift right by 8 the value of $? ($? >> 8).
#If the value of $? is -1, then the command failed to execute, in that case you may check the value of $! for the reason of the failure.    
    system ($command);
    if ($? == -1) {
	print "system call in $0 with $command failed to execute: $!\n";
	exit(1);
    }
    elsif ($? & 127) {
     	printf "system call in $0 with $command child died with signal %d, %s coredump\n",
     	    ($? & 127),  ($? & 128) ? 'with' : 'without';
     	exit(1);
    }
    else {
	unless($? >> 8 == 0) {
	    printf "system call in $0 with $command child exited with value %d\n", $? >> 8;
	    exit(1);
	} else {
	    ## fine system call
	}
    }
}


sub usage {
print STDERR <<USAGE;
usage:  $0 -d|directory -b|bwa_index -r|re -bed [-F|force] [-f|--fork] [-s|sra] [-v|--verbose] [-V|--version] [-h|--help|?] 
        -d or --directory : input directory with fastq and/or sra files
        -bed bedfile
        -b or --bwa_index : filename of bwaindex such as /home/harmen/data/genomes/hsapiens/hg19/hg19_merge
        -f or fork : number cpus used
        -F or force : overwrite files
        -p or pe or paired-end : paired-end sequencing
        -q or qpoor: poor quality 3'end bwa aln with -q 20
        -r or re:  restriction enzyme name
        --h or ? or --help : help
        -v or --verbose : verbose output
        -V or Version : version
USAGE
exit
}

