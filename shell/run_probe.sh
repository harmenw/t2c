#!/bin/bash
#ibam=/media/bigmama/lala/data/THiC/all_new/old_PE/Beta-globin_FL_trim_HindIII_enzdb_HindIII_NlaIII_PE.bam
idir=/media/bigmama/lala/data/T2C/
#idir=/data/THiC/testset/
odir=${idir}/probes_count/
mkdir -p ${odir}
gdir=/data/genomes
dirsites=${gdir}/hsapiens/hg18/enzdb/
hfile=${dirsites}/hg18_BglII2NlaIII.bed.gz

## run what program
# coverage file  take colums 7 to know how many base are covered
if [ ! -s ${hfile} ]  || [ -z $(gzip -cd ${hfile} | head -c1) ] ; then 
   echo ${hfile}
   zcat ${dirsites}hg18_AGATCT_CATG.gz|  tail -n +2 |  cut -f 3,8,9 | perl -lane ' print join("\t",$F[0],$F[1]-1,$F[2])' | sort | uniq | sort -k 1,1 -k2,2n -k3,3n | gzip  > ${hfile}
fi

# covfile=/data/THiC/Oligos_on_the_array/120131_HG18_BglII_TK_cap_probe_sequences_BglII_NlaIII_coverage.bed
# if [ ! -f ${covfile} ]; then 
# echo skip
# #    coverageBed -a ${hfile}  -b /data/THiC/Oligos_on_the_array/120131_HG18_BglII_TK_cap_probe_sequences.bed  > ${covfile}
# fi

dirsites=${gdir}/mmusculus/mm9/enzdb/
mfile=${dirsites}mm9_HindIII2NlaIII.bed.gz
if [ ! -s ${mfile} ] || [ -z $(gzip -cd ${mfile} | head -c1) ] ; then 
   echo ${mfile}
   zcat ${dirsites}mm9_AAGCTT_CATG.gz|  tail -n +2 |  cut -f 3,8,9 | perl -lane ' print join("\t",$F[0],$F[1]-1,$F[2])' | sort | uniq | sort -k 1,1 -k2,2n -k3,3n | gzip  > ${mfile}
fi

mfile=${dirsites}mm9_HindIII2DpnII.bed.gz
if [ ! -s ${mfile} ] || [ -z $(gzip -cd ${mfile} | head -c1) ] ; then 
   echo ${mfile}
   zcat ${dirsites}mm9_AAGCTT_GATC.gz|  tail -n +2 |  cut -f 3,8,9 | perl -lane ' print join("\t",$F[0],$F[1]-1,$F[2])' | sort | uniq | sort -k 1,1 -k2,2n -k3,3n | gzip  > ${mfile}
fi


pdir=/data/THiC/Oligos_on_the_array/
# covfile=/data/THiC/Oligos_on_the_array/120127_MM9_HindIII_TK_cap_probe_sequences_HindII_NlaIII_coverage.bed
# if [ ! -f ${covfile} ]; then 
# echo skip
# #    coverageBed -a ${mfile}  -b /data/THiC/Oligos_on_the_array/120127_MM9_HindIII_TK_cap_probe_sequences.bed  > ${covfile}
# fi
#fi
#popd

## notebed tools use 0 as start!
## in sam file is 1 based based
## not a big problem because little fragments start with restriction site and should be reduced but it i

pushd ${idir}
list=($(find . -name "*qc_[12]_trim*bam" ! -regex ".*enzdb.*" ! -regex ".*_PE\\.bam" | sort))
for (( i = 0; i < ${#list[@]}; i = i + 2)) 
do
#    echo ${list[${i}+1]}
    expname=$(echo ${list[${i}]} | sed -e 's/\.bam//'| sed -e 's/.*\///'| sed -e 's/_1_/_/')
## if hg18 then else mm9 binning
    if [[ ${list[${i}]} =~ "hg18" ]];  then
	direnzdb=${gdir}/hsapiens/hg18/enzdb/
	enzdbfile=${direnzdb}hg18_BglII2NlaIII.bed.gz 	
	enzdbfilefull=${direnzdb}hg18_BglII2BglII.bed.gz 	
	olifile=${pdir}120131_HG18_BglII_TK_cap_probe_sequences.bed.gz
    elif [[ ${expname} =~ "globin" ]];  then
	direnzdb=${gdir}/mmusculus/mm9/enzdb/
	enzdbfile=${direnzdb}mm9_HindIII2NlaIII.bed.gz
	enzdbfilefull=${direnzdb}mm9_HindIII2HindIII.bed.gz 	
	olifile=${pdir}120127_MM9_HindIII_TK_cap_probe_sequences.bed.gz
    elif [[ ${expname} =~ "Myb_Bcl11a" ]];  then
	direnzdb=${gdir}/mmusculus/mm9/enzdb/
	enzdbfile=${direnzdb}mm9_HindIII2DpnII.bed.gz
	enzdbfilefull=${direnzdb}mm9_HindIII2HindIII.bed.gz 	
	olifile=${pdir}OID40170_HindIII_Myb_Bcl11a.bed.gz
    elif [[ ${expname} =~ "Runx1_" ]];  then
	direnzdb=${gdir}/mmusculus/mm9/enzdb/
	enzdbfile=${direnzdb}mm9_HindIII2DpnII.bed.gz
	enzdbfilefull=${direnzdb}mm9_HindIII2HindIII.bed.gz 	
	olifile=${pdir}OID40169_HindIII_Runx1_mm9.bed.gz
    else 
	echo " Don't know what to to with this file ${list[${i}]}"
	exit 1
    fi;
    echo ${expname}
    echo ${list[${i}]}
    echo ${enzdbfile}
    echo ${enzdbfilefull}
    echo ${olifile}
##    intersectBed -abam <(samtools merge - hg18/output/sam/HB2_test_1_trim_BglII.bam hg18/output/sam/HB2_test_2_trim_BglII.bam) -b /data/THiC/sites/hg18_BglII2NlaIII.bed -f 0.9 > /tmp/test.bam
    if [ ! -s ${odir}${expname}_full_fragment.bed.gz ] || [ -z $(gzip -cd ${odir}${expname}_full_fragment.bed.gz | head -c1) ] ; then 
	echo ${olifile}
# intersect reads fully with database and put it in bed file
	intersectBed -abam <(samtools merge - ${list[${i}]} ${list[${i} +1]}) -b ${enzdbfile} -f 1 -bed > /tmp/${expname}_test.bed
## intersect reads remove reads with overlap less than 6 bases # hindfragment count fragments  and different order
	intersectBed -a ${enzdbfile} -b /tmp/${expname}_test.bed -wo  |  awk '$16>6' | cut -f 1-3 |  sort | uniq -c |  awk 'BEGIN {OFS="\t"};{print $2,$3,$4,".",$1}'  | sort -k1,1 -k2,2n > /tmp/${expname}_test2.bed
# no zeros 
# add with probes however some probes have two on restriction fragment-end
	## maybe 0.90 although not much differences
	intersectBed -a /tmp/${expname}_test2.bed -b ${olifile} -wao   | perl -lane '$F[10] = 0 if $F[10] <= 40; print join("\t",@F);' > /tmp/${expname}_test_end_result.bed
## with number of probes set into name columns
	perl -lane '$t = $F[10] == 0  ? 0 :1; $hash{join("\t",@F[0..2],$F[4])} += $t; END {foreach $key (keys %hash) { print $key,"\t",$hash{$key}} }' /tmp/${expname}_test_end_result.bed | perl -lane 'print join("\t",@F[0..2],$F[4],$F[3])'  | sort -k1,1 -k2,2n -k3,3n | gzip > ${odir}${expname}_fragment_end.bed.gz
#
	intersectBed -a ${odir}${expname}_fragment_end.bed.gz -b ${enzdbfilefull} -wao | perl -lane ' next if $F[8] <=6; $hash{join("\t",@F[5..7])}[0] += $F[3]; $hash{join("\t",@F[5..7])}[1] += $F[4]; ++$hash{join("\t",@F[5..7])}[2] ; END {foreach $key (keys %hash) { print join("\t",$key,$hash{$key}[0],$hash{$key}[1],"+",$hash{$key}[2]) }}' | sort -k1,1 -k2,2n |gzip > ${odir}${expname}_full_fragment.bed.gz
	rm /tmp/${expname}_test.bed /tmp/${expname}_test2.bed /tmp/${expname}_test_end_result.bed
    fi;
## merge bams already uniq
##samtools merge - hg18/output/sam/HB2_test_1_trim_BglII.bam hg18/output/sam/HB2_test_2_trim_BglII.bam | samtools view - |wc -l
done
popd

exit
